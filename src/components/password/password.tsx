'use client'
import { ArrowLeftOutlined } from '@ant-design/icons'
import Link from 'next/link'
import { usePathname } from 'next/navigation'
import { Button, Input, Label } from '../common'
import { useForm } from 'react-hook-form'
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { messageValidate } from '@/constants'
const { MATCH, MIN, REQUIRE } = messageValidate

type PasswordChange = {
  password: string
  newpass: string
  confirm: string
}
type RegisterForm = keyof PasswordChange

const fakeChangePassword = [
  { id: 1, title: 'Current Password', inputName: 'password' },
  { id: 2, title: 'New Password', inputName: 'newpass' },
  { id: 3, title: 'Confirm Password', inputName: 'confirm' },
]

const schema = yup.object({
  password: yup.string().required(REQUIRE),
  newpass: yup.string().required(REQUIRE).min(5, MIN),
  confirm: yup
    .string()
    .required(REQUIRE)
    .oneOf([yup.ref('newpass')], MATCH),
})

const PasswordComponent = () => {
  const prevPath = usePathname().split('/')[1]
  const {
    register,
    formState: { errors, isSubmitting },
    handleSubmit,
  } = useForm<PasswordChange>({
    resolver: yupResolver(schema),
  })

  const onChangePassword = async (values: PasswordChange) => {
    console.log('🚀 ~ onChangePassword ~ values:', values)
  }
  return (
    <section className='mt-24 bg-[#e8f1fa] flex min-h-[calc(100vh-96px)] justify-center'>
      <article className='bg-white max-w-full w-[800px] rounded-md h-max lg:mt-5 lg:m-0 m-5'>
        {/* header */}
        <Link
          href={`/${prevPath}`}
          className='bg-purple-600 text-white w-full p-6 inline-flex gap-3 items-center rounded-md'
        >
          <ArrowLeftOutlined className='text-[22px]' />
          <h4 className='font-semibold text-lg'>Change Password</h4>
        </Link>

        {/* form */}
        <form
          autoComplete='off'
          className='p-4 lg:p-10'
          onSubmit={handleSubmit(onChangePassword)}
        >
          {fakeChangePassword.map((item) => (
            <div key={item.id} className='form-group mb-5'>
              <Label
                classnames='!text-[#515184] font-semibold text-sm'
                htmlFor={item.inputName}
              >
                {item.title}
              </Label>
              <Input
                id={item.inputName}
                {...register(item.inputName as RegisterForm)}
                name={item.inputName}
              />
              {errors && errors[item.inputName as RegisterForm] && (
                <p className='text-red-500 text-sm'>
                  {errors[item.inputName as RegisterForm]?.message}
                </p>
              )}
            </div>
          ))}

          <Button
            type='submit'
            disabled={isSubmitting}
            color='secondary'
            classnames='w-[175px]'
          >
            {isSubmitting ? 'Loading' : 'Save'}
          </Button>
        </form>
      </article>
    </section>
  )
}

export default PasswordComponent
