'use client'
import { AVATAR, COVER_IMG, menuProfileList } from '@/constants'
import Image from 'next/image'
import { Button, PostedComponent, CreatedPost } from '../common'
import {
  EllipsisOutlined,
  MailOutlined,
  UserAddOutlined,
} from '@ant-design/icons'
import Link from 'next/link'
import { usePathname, useSearchParams } from 'next/navigation'

const activeStyle = 'border-b-2 border-solid border-[#333] text-gray-900'

export default function ProfileComponent() {
  const pathname = usePathname()
  const searchParams = useSearchParams()
  const tab = searchParams.get('tab')

  const isActive = (href: string): string => {
    return tab === href ? activeStyle : 'text-gray-400'
  }

  return (
    <section className='bg-gray-100 md:px-0 px-5 mt-24 flex justify-center min-h-screen'>
      <div className='hero w-[990px] max-w-full'>
        {/* Banner and profile image */}
        <div className='bg-white relative h-[375px] mt-5 rounded-xl shadow-md overflow-hidden'>
          {/* banner */}
          <div className='h-1/2 w-full relative'>
            <Image
              src={COVER_IMG}
              alt='cover-image'
              fill
              className='object-cover '
              sizes='(min-width : 768px) 50vw , 100vw'
              quality={85}
            />
          </div>

          {/* profile image */}
          <div className='flex items-end justify-between px-4 absolute w-full -translate-y-8'>
            <div className='flex items-end gap-5'>
              <Image
                src={AVATAR}
                alt='avatar-info'
                width={100}
                height={100}
                className='object-cover rounded-full border-[6px] border-gray-100'
              />
              <div className='name_profile'>
                <h2 className='text-black text-xl font-bold'>Example Human</h2>
                <span className='text-gray-300 text-sm'>example@gmail.com</span>
              </div>
            </div>

            <div className='flex items-center gap-3 justify-end'>
              <Button color='success' classnames='p-3 w-12 !h-12 text-xs'>
                <span className='flex-shrink-0 font-semibold text-lg flex items-center justify-center'>
                  <UserAddOutlined />
                </span>
              </Button>
              <Button color='light' classnames='p-3 w-12 !h-12 text-gray-600'>
                <span className='flex-shrink-0 font-semibold text-lg flex items-center justify-center'>
                  <MailOutlined />
                </span>
              </Button>
              <Button color='light' classnames='p-3 w-12 !h-12 text-gray-600'>
                <span className='flex-shrink-0 font-semibold text-lg flex items-center justify-center'>
                  <EllipsisOutlined />
                </span>
              </Button>
            </div>
          </div>

          {/* tab action */}
          <div className='flex px-6 mt-[120px] items-center gap-10 border-t-2 border-t-[#f1f1f1] border-solid'>
            {menuProfileList.map((menu) => (
              <Link
                className={`font-bold py-[22px] hover:text-gray-200 text-sm ${isActive(
                  menu.href,
                )}`}
                href={`${pathname}?tab=${menu.href}`}
                key={menu.id}
              >
                {menu.title}
              </Link>
            ))}
          </div>
        </div>

        {/* Profile Hero */}
        <article className='grid grid-cols-1 grid-flow-row md:grid-cols-3 gap-4 mt-5'>
          <div className='md:col-span-1 flex flex-col gap-4'>
            {/* about */}
            <div className='about bg-white h-max shadow-md p-3 rounded-xl'>
              <h4 className='font-bold text-sm mb-5'>About</h4>
              <p className='text-gray-400 font-semibold text-xs pb-5 leading-loose'>
                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Illum
                cumque sunt distinctio alias? A qui officiis neque, voluptatem
                aliquid nulla error at, repudiandae vero quod deserunt culpa,
                illo corporis aspernatur!
              </p>
            </div>

            {/* photo */}
            <div className='photo-images bg-white h-max shadow-md p-3 rounded-xl'>
              <div className='flex items-center mb-3 font-bold text-sm justify-between'>
                <h4>Photos</h4>
                <Link
                  className='text-purple-500 hover:text-gray-300'
                  href={`${pathname}?tab=media`}
                >
                  See all
                </Link>
              </div>
              {/* list photo */}
              <div className='grid grid-cols-2 gap-2'>
                {Array(6)
                  .fill(null)
                  .map((_, index) => (
                    <div
                      key={`_${index}`}
                      className='w-full rounded-md overflow-hidden h-[120px] relative'
                    >
                      <Image
                        fill
                        sizes='(max-width: 120px) 100px, 120px'
                        className='object-cover'
                        src={`https://placehold.co/600x400/png?text=${index}`}
                        alt='photo'
                      />
                    </div>
                  ))}
              </div>

              <Button classnames='rounded-lg text-black w-full my-3'>
                More Photos
              </Button>
            </div>
          </div>

          {/* create post */}
          <div className='md:col-span-2 flex flex-col gap-4 mb-4'>
            <CreatedPost />

            {/* posted */}
            <PostedComponent />
            <PostedComponent />
          </div>

          {/* posted */}
        </article>
      </div>
    </section>
  )
}
