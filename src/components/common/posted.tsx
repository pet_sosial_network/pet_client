'use client'

import Image from 'next/image'
import {
  CommentOutlined,
  LikeFilled,
  LikeOutlined,
  ShareAltOutlined,
} from '@ant-design/icons'
import { useState } from 'react'
import { Author } from './author'

export function PostedComponent() {
  const [like, setLike] = useState<boolean>(false)
  const handleLikesPost = () => setLike((prev) => !prev)
  return (
    <div className='posted h-max bg-white shadow-md px-6 py-4 rounded-xl'>
      <Author name='Tony Anum' time='3 minutes ago' hasOption />

      <div className='content-post'>
        <h3 className='font-semibold text-sm text-gray-400 mt-3 leading-[2]'>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio
          exercitationem obcaecati sit laborum reiciendis saepe dolor placeat
          non tenetur, cum a repellendus quaerat quibusdam numquam quas sunt
          quidem pariatur est.
        </h3>

        <div className='flex item-center gap-2 photo mt-4 overflow-hidden'>
          {Array(3)
            .fill(0)
            .map((_, index) => (
              <Image
                key={index}
                width={195}
                height={127}
                src='/images/post.jpg'
                className='object-cover rounded-md'
                alt={`photo-${index}`}
              />
            ))}
        </div>

        <div className='icon-action flex item-center gap-4 my-4'>
          <span
            onClick={handleLikesPost}
            className='flex items-end text-gray-600 cursor-pointer gap-2 text-3xl font-medium'
          >
            {like ? <LikeFilled /> : <LikeOutlined />}
            <span className='text-sm text-gray-500 select-none'>
              2.8k Likes
            </span>
          </span>

          <span className='flex items-end text-gray-600 cursor-pointer gap-2 text-3xl font-medium'>
            <CommentOutlined />
            <span className='text-sm text-gray-500'>22 comments</span>
          </span>

          <span className='flex items-end text-gray-600 cursor-pointer ml-auto gap-2 text-3xl font-medium'>
            <ShareAltOutlined />
            <span className='text-sm text-gray-500'>Share</span>
          </span>
        </div>
      </div>
    </div>
  )
}
