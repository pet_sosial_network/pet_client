type LabelProps = {
  children: React.ReactNode
  classnames?: string
} & JSX.IntrinsicElements['label']

export const Label = (props: LabelProps) => {
  const { classnames = '', children, ...rest } = props
  const defaultStyle =
    'text-[#adb5bd] font-light select-none cursor-pointer dark:text-gray-300'
  return (
    <label className={`${defaultStyle} ${classnames}`} {...rest}>
      {children}
    </label>
  )
}
