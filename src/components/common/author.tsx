import { AVATAR } from '@/constants'
import { EllipsisOutlined } from '@ant-design/icons'
import Image from 'next/image'

interface AuthorProps {
  images?: string
  name: string
  time?: Date | string
  hasOption?: boolean
  size?: 'chat' | 'post'
  isSender?: boolean
}

const sizeAvatar = {
  post: 'w-[45px] h-[45px]',
  chat: 'w-10 h-10',
}

export function Author({
  images = AVATAR,
  name,
  time,
  hasOption = false,
  size = 'post',
  isSender = false,
}: AuthorProps) {
  return (
    <div
      className={`flex items-center gap-x-[10px] ${
        isSender && 'flex-row-reverse'
      }`}
    >
      <div
        className={` ${sizeAvatar[size]} rounded-full relative overflow-hidden`}
      >
        <Image
          src={images}
          alt='avatar'
          fill
          sizes='(max-width: 120px) 40px, 45px'
          className='object-cover'
        />
      </div>

      <div className='name-time flex flex-col gap-y-[2px]'>
        <p className='font-bold text-sm'>{name}</p>
        <span
          className={`font-medium text-gray-400 text-xs ${
            isSender && 'ml-auto'
          }`}
        >
          {time as string}
        </span>
      </div>

      {hasOption && (
        <div className='w-[45px] flex items-center justify-center h-[45px] rounded-full bg-gray-200 ml-auto hover:bg-gray-400 text-2xl cursor-pointer'>
          <EllipsisOutlined />
        </div>
      )}
    </div>
  )
}
