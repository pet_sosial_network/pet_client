'use client'
import { AVATAR } from '@/constants'
import { profileSubMenu } from '@/constants/static'
import Image from 'next/image'
import Link from 'next/link'
import { useState } from 'react'

export default function Profile() {
  const [show, setShow] = useState<boolean>(false)
  const toggleShow = () => {
    setShow((prev) => !prev)
  }
  return (
    <div className='relative'>
      <div className='object-cover rounded-full w-9 h-9 cursor-pointer relative'>
        <Image
          onClick={toggleShow}
          className='rounded-full'
          src={AVATAR}
          fill
          alt='user profile'
        />
      </div>

      {/* Dropdown */}
      {show && (
        <div className='absolute right-0 z-10 w-56 mt-4 origin-top-right bg-white border border-gray-100 rounded-md shadow-lg'>
          <div className='p-2'>
            {profileSubMenu.map((menu) => (
              <Link
                key={menu.id}
                href={menu.href}
                className='flex font-medium items-center gap-2 px-4 py-2 text-gray-500 rounded-lg hover:bg-gray-50 hover:text-gray-700'
              >
                <span className='text-lg mb-1 text-blue-500'>{menu.icon}</span>
                <span>{menu.title}</span>
              </Link>
            ))}
          </div>
        </div>
      )}
    </div>
  )
}
