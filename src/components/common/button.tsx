type ButtonProps = {
  children: React.ReactNode
  classnames?: string
  color?: 'primary' | 'secondary' | 'success' | 'light'
} & JSX.IntrinsicElements['button']

export const Button = (props: ButtonProps) => {
  const { classnames = '', children, color = 'primary', ...rest } = props
  const defaultStyle =
    'rounded-md h-14 hover:bg-slate-600 text-white font-semibold'
  const styleColor = {
    primary: 'bg-[#343a40]',
    secondary: 'bg-purple-500',
    success: 'bg-[#ccd472]',
    light: 'bg-[#efe5e5]',
  }
  return (
    <button
      className={`${defaultStyle} ${classnames} ${styleColor[color]}`}
      {...rest}
    >
      {children}
    </button>
  )
}
