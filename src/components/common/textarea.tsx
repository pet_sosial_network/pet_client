type TextAreaProps = {
  classnames?: string
} & JSX.IntrinsicElements['textarea']

export function Textarea(props: TextAreaProps) {
  const { classnames, ...rest } = props
  const defaultStyle =
    'block p-2.5 w-full outline-none border-2 border-gray-200 text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 transition-all duration-250 focus:border-purple-500'
  return (
    <textarea className={`${defaultStyle} ${classnames}`} {...rest}></textarea>
  )
}
