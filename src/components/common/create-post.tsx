import Image from 'next/image'
import { AVATAR } from '@/constants'
import { EditOutlined, VideoCameraOutlined } from '@ant-design/icons'

export function CreatedPost() {
  return (
    <div className='create-post h-max bg-white shadow-md px-6 py-4 rounded-xl'>
      <div className='flex items-center gap-2 mb-5'>
        <span className='text-xl grid place-items-center w-10 h-10 rounded-full text-purple-500 bg-gray-200'>
          <EditOutlined />
        </span>
        <span className='text-gray-400 font-bold text-sm'>Create post</span>
      </div>

      <div className='flex items-start gap-3 p-2 border-gray-200 border-2 rounded-lg'>
        <Image
          src={AVATAR}
          alt='user'
          width={30}
          height={30}
          className='rounded-full object-cover'
        />

        <textarea
          rows={5}
          className='w-full text-sm outline-none border-transparent'
          placeholder="What's on your mind?"
        ></textarea>
      </div>

      <div className='flex items-center gap-5 mt-5'>
        <div className='flex items-center gap-2'>
          <span className='text-xl text-gray-100 grid place-items-center w-10 h-10 rounded-full bg-[#ccd472]'>
            <VideoCameraOutlined />
          </span>
          <span className='font-bold text-gray-400 text-sm'>Live Video</span>
        </div>

        <div className='flex items-center gap-2'>
          <span className='text-xl text-gray-100 grid place-items-center w-10 h-10 rounded-full bg-[#ccd472]'>
            <VideoCameraOutlined />
          </span>
          <span className='font-bold text-gray-400 text-sm'>Photo/Video</span>
        </div>

        <div className='flex items-center gap-2'>
          <span className='text-xl text-gray-100 grid place-items-center w-10 h-10 rounded-full bg-[#ccd472]'>
            <VideoCameraOutlined />
          </span>
          <span className='font-bold text-gray-400 text-sm'>
            Activity/Feeling
          </span>
        </div>
      </div>
    </div>
  )
}
