import { MenuOutlined, SearchOutlined } from '@ant-design/icons'
import { Input } from '.'
import AppLogo from './logo'
import { navBarActions } from '@/constants/static'
import Link from 'next/link'
import Profile from './profile'

export function Header() {
  return (
    <header className='w-full h-24 fixed md:px-3 px-4 bg-white top-0 z-10 left-0 right-0 flex items-center justify-between border border-gray-200 shadow-md'>
      {/* Logo */}
      <AppLogo />

      {/* Search */}
      <div className='md:flex items-center text-sm hidden px-3 gap-2 bg-gray-200 h-12 w-[350px] rounded-full'>
        <span className='text-gray-400 text-lg mb-2'>
          <SearchOutlined />
        </span>
        <Input
          className='border-none p-2 bg-gray-200 w-full outline-none focus:border-transparent'
          placeholder='Search post, user, ...'
        />
      </div>

      {/* Profile */}
      <div className='flex items-center justify-center gap-5'>
        <div className='hidden md:flex md:items-center md:gap-5'>
          {navBarActions.map((item) => (
            <Link
              href={item.href}
              title={item.title}
              key={item.id}
              className='text-blue-500 text-3xl font-semibold'
            >
              {item.icon}
            </Link>
          ))}
        </div>
        <span className='flex items-center justify-center hover:bg-gray-200 p-3 rounded-md md:hidden text-xl cursor-pointer font-bold'>
          <MenuOutlined />
        </span>
        <Profile />
      </div>
    </header>
  )
}
