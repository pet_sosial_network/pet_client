import {
  AudioOutlined,
  CameraOutlined,
  LinkOutlined,
  MehOutlined,
  SendOutlined,
} from '@ant-design/icons'
import { Input } from '.'

export function ChatInput() {
  return (
    <div className='border-gray-200 px-4 mb-2 sm:mb-0'>
      <div className='relative flex'>
        <span className='absolute inset-y-0 flex items-center'>
          <button
            type='button'
            className='inline-flex items-center justify-center rounded-full h-12 w-12 transition duration-500 ease-in-out text-gray-800 hover:bg-gray-300 focus:outline-none'
          >
            <AudioOutlined />
          </button>
        </span>
        {/* input */}
        <div className='w-full text-gray-600 placeholder-gray-600 pl-12 bg-gray-200 rounded-md py-3'>
          <input
            type='text'
            placeholder='Write your message!'
            className='focus:outline-none w-[500px] focus:placeholder-gray-400 bg-transparent'
          />
        </div>
        {/* icon actions */}
        <div className='absolute right-0 items-center inset-y-0 hidden sm:flex'>
          <button
            type='button'
            className='inline-flex text-lg text-black items-center justify-center rounded-full h-10 w-10 transition duration-500 ease-in-out hover:bg-gray-300 focus:outline-none'
          >
            <LinkOutlined />
          </button>
          <button
            type='button'
            className='inline-flex text-lg text-black items-center justify-center rounded-full h-10 w-10 transition duration-500 ease-in-out hover:bg-gray-300 focus:outline-none'
          >
            <CameraOutlined />
          </button>
          <button
            type='button'
            className='inline-flex text-lg text-black items-center justify-center rounded-full h-10 w-10 transition duration-500 ease-in-out hover:bg-gray-300 focus:outline-none'
          >
            <MehOutlined />
          </button>
          <button
            type='button'
            className='rounded-lg px-4 py-3 transition duration-500 ease-in-out text-white bg-blue-500 hover:bg-blue-400 focus:outline-none'
          >
            <span className='font-bold'>Send</span>
          </button>
        </div>
      </div>
    </div>
  )
}
