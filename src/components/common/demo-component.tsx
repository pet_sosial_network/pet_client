'use client'

import styled from 'styled-components'

const Header = styled.h2`
  font-size: 32px;
  font-weight: 600;
`

export function DemoComponent() {
  return (
    <div className='flex-col items-center justify-center w-2/3 h2/3'>
      <Header className='text-center text-blue-400'>Demo Component</Header>
      <p className='text-center'>
        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequatur
        veritatis deleniti a quos repellendus voluptatibus quam veniam animi
        excepturi? Vitae nam voluptatum eligendi. Animi, officia. Minima animi
        quis quas cum.
      </p>
    </div>
  )
}
