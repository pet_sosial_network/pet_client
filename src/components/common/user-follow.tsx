import Image from "next/image"
import { fakeUserFollowInfo } from "@/constants/static"
import { Swiper, SwiperSlide } from "swiper/react"
import 'swiper/css';
import { Pagination } from 'swiper/modules';

export type UserFollowInfo = {
    name: string
    id: string
}
export function UserFollow() {
    const handleUserFollowInfo = () => {
        if (!fakeUserFollowInfo) return []
        if (!Array.isArray(fakeUserFollowInfo)) return []
        return fakeUserFollowInfo
    }
    handleUserFollowInfo()
    return (
        <Swiper
            slidesPerView={4}
            spaceBetween={10}
            className="userFollowSlider cursor-grab"
            modules={[Pagination]}
        >
            {fakeUserFollowInfo.map((user, index) => (
                <SwiperSlide key={index}>
                    <div className="users-center shadow-md w-[150px] h-full relative rounded-xl text-center bg-white">
                        <Image
                            src={user.avatar}
                            width={75}
                            height={75}
                            alt="User's image"
                            className="object-cover rounded-full border-[6px] border-white mx-[25%]"
                        />
                        <div className='mt-2 p-2'>
                            <h2 className='text-black text-sm font-bold'>{user.name}</h2>
                            <span className='text-gray-300 text-[10px]'>{user.id}</span>
                        </div>
                        <button className="bg-green-400 rounded-full text-white text-[10px] font-bold uppercase px-4 py-2 mb-2">follow</button>
                    </div>
                </SwiperSlide>
            ))}
        </Swiper>
    )
}