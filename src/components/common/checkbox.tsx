'use client'
import { InputProps } from '@/types'
import { Fragment, LegacyRef, forwardRef } from 'react'
import styled from 'styled-components'

const CustomCheckbox = styled.input`
  &::before {
    content: '';
    position: absolute;
    display: inline-block;
    width: 12px;
    height: 8px;
    rotate: -45deg;
    left: 3px;
    top: 3px;
  }
  &:checked::before {
    border: 3px solid;
    border-color: white;
    border-top-color: transparent;
    border-right-color: transparent;
    border-radius: 2px;
  }
`

const MyCheckbox = (props: InputProps, ref: LegacyRef<HTMLInputElement>) => {
  const { classnames = '', ...rest } = props
  const defaultStyle =
    'relative checked:ring-4 checked:ring-blue-300 w-5 h-5 bg-white border border-[#ccc] rounded appearance-none checked:bg-purple-500'
  return (
    <Fragment>
      <CustomCheckbox
        ref={ref}
        type='checkbox'
        className={`${defaultStyle} ${classnames}`}
        {...rest}
      ></CustomCheckbox>
    </Fragment>
  )
}

const Checkbox = forwardRef(MyCheckbox)
export { Checkbox }
