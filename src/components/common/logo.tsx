import { ThunderboltOutlined } from '@ant-design/icons'
import Link from 'next/link'
import React from 'react'

const AppLogo = () => {
  return (
    <Link href='/' className='flex items-center justify-center text-3xl gap-2'>
      <span className='text-[#10d876]'>
        <ThunderboltOutlined />
      </span>
      <h1 className='font-bold text-blue-600'>Sociala.</h1>
    </Link>
  )
}

export default AppLogo
