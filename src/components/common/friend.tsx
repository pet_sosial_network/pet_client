import Image from "next/image"
import { fakeListFriends } from "@/constants/static"

export type Friend = {
    avatar: string
    name: string
    numMutualFriends: number
}
export function Friend() {
    const handleFriendInfo = () => {
        if (!fakeListFriends) return []
        if (!Array.isArray(fakeListFriends)) return []
        return fakeListFriends
    }
    handleFriendInfo()
    
    return(
        <div>
            {fakeListFriends.map((friend, index) => (
                <div key={index} className="pl-6 py-4 mr-[25%]">
                    <Image
                        src={friend.avatar}
                        width={45}
                        height={45}
                        alt="Friend's image"
                        className="object-cover rounded-full"
                    />
                    <div className="-mt-11 md:float-right text-[14px] ml-14">
                        <h4 className="font-bold mr-4 w-full">{friend.name}</h4>
                        <p className="text-gray-400">{friend.numMutualFriends} mutual friends</p>
                    </div> 
                    <div className="pl-6 mt-4 text-xs flex -ml-5">
                        <button className="bg-blue-600 text-white rounded-full px-5 py-2 font-bold">Confirm</button>
                        <button className="bg-gray-200 rounded-full px-6 py-2 font-bold ml-4">Delete</button>
                    </div>
                </div>
            ))}
        </div>
                       
        )
}