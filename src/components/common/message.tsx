import { ReactNode } from 'react'

interface MessageProps {
  children: ReactNode
  isSender?: boolean
}

export function Message({ children, isSender = false }: MessageProps) {
  return (
    <p
      className={`p-3 rounded-lg text-sm w-max font-medium ${
        isSender ? 'bg-[#05f] text-white' : 'bg-[#cdddfb]'
      }`}
    >
      {children}
    </p>
  )
}
