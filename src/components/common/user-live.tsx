import Image from "next/image"
import { fakeUserLiveInfo } from "@/constants/static"
import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css';
import { Pagination } from 'swiper/modules';

export type UserLiveInfo = {
    avatar: string
    background: string
    name: string
    gmail: string
}
export function UserLive() {
    const handleUserLiveInfo = () => {
        if (!fakeUserLiveInfo) return []
        if (!Array.isArray(fakeUserLiveInfo)) return []
        return fakeUserLiveInfo
    }
    handleUserLiveInfo()
    return (   
        <Swiper
            slidesPerView={3}
            spaceBetween={10}
            className="userLiveSlider cursor-grab"
            modules={[Pagination]}
        >
        {fakeUserLiveInfo.map((user,index) => (
            <SwiperSlide key={index}>
                <div className="users-center shadow-md w-[200px] h-full relative rounded-xl text-center bg-white">
                    <img src={user.background} className="rounded-t-xl w-full h-[100px] object-cover"></img>
                    <Image
                        src={user.avatar}
                        width={75}
                        height={75}
                        alt="User's image"
                        className="object-cover rounded-full border-[6px] border-white absolute -mt-12 mx-[30%]"
                    />
                    <div className='mt-5 p-2 bg-white'>
                        <h2 className='text-black text-sm font-bold'>{user.name}</h2>
                        <span className='text-gray-300 text-[10px]'>{user.gmail}</span>
                    </div>
                    <button className="bg-red-600 rounded-md text-white text-[10px] font-bold uppercase p-2 mb-2">live</button>
                </div> 
            </SwiperSlide>     
        ))}
        </Swiper>
)}