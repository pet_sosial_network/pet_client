import { InputProps } from '@/types'
import { LegacyRef, forwardRef } from 'react'

const MyInput = (props: InputProps, ref: LegacyRef<HTMLInputElement>) => {
  const { classnames = '', ...rest } = props
  const defaultStyle =
    'border-2 border-[#eeeeee] transition-all duration-250 focus:border-purple-500 font-medium rounded-md w-full outline-none h-14 px-5 placeholder:font-medium text-[#212529]'
  return (
    <input ref={ref} className={`${defaultStyle} ${classnames}`} {...rest} />
  )
}

const Input = forwardRef(MyInput)
export { Input }
