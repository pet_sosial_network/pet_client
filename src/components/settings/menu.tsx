'use client'
import { MenuStatics } from '@/types'
import { RightOutlined } from '@ant-design/icons'
import Link from 'next/link'
import { usePathname } from 'next/navigation'
import React, { Fragment } from 'react'

const MenuSettings = ({
  menus,
  title,
}: {
  menus: MenuStatics[]
  title: string
}) => {
  const pathname = usePathname()

  return (
    <Fragment>
      <p className='font-semibold text-gray-500 text-sm mb-5'>{title}</p>
      <ul className='mb-4 flex flex-col gap-y-2'>
        {menus.map((item) => (
          <li
            key={item.id}
            className='border-b-2 hover:text-purple-400 h-14 border-solid border-[#e1e1f0]'
          >
            <Link
              className='flex items-center gap-3'
              href={`${pathname}/${item.href}`}
            >
              <span className='flex text-white items-center justify-center w-11 h-11 rounded-full bg-purple-400'>
                {item.icon}
              </span>
              <span className='font-semibold text-sm'>{item.title}</span>
              <span className='text-[#6969a3] ml-auto'>
                <RightOutlined />
              </span>
            </Link>
          </li>
        ))}
      </ul>
    </Fragment>
  )
}

export default MenuSettings
