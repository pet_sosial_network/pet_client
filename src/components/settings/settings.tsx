import { settingsMenulist } from '@/constants/static'
import MenuSettings from './menu'

const { Account, General, Other } = settingsMenulist
export default function SettingsComponents() {
  return (
    <section className='max-w-full mt-24 bg-[#c9c9d6] min-h-screen flex justify-center'>
      <div className='w-[800px] md:mx-0 mx-4 mt-3 bg-white rounded-md py-10 px-8'>
        {/* header */}
        <h1 className='font-bold text-3xl mb-10'>Settings</h1>

        {/* menu need loop */}
        <MenuSettings menus={General} title='General' />
        <MenuSettings menus={Account} title='Account' />
        <MenuSettings menus={Other} title='Other' />
      </div>
    </section>
  )
}
