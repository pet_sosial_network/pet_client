'use client'
import { AVATAR, fakeFormData } from '@/constants'
import { ArrowLeftOutlined } from '@ant-design/icons'
import Image from 'next/image'
import Link from 'next/link'
import { Button, Input, Label, Textarea } from '../common'
import { usePathname } from 'next/navigation'

export default function AccountComponent() {
  const prevPath = usePathname().split('/')[1]
  return (
    <section className='mt-24 bg-[#e8f1fa] min-h-screen flex justify-center'>
      <article className='max-w-full w-[800px] lg:m-0 lg:mt-5 m-5 bg-white rounded-md'>
        {/* header */}
        <Link
          href={`/${prevPath}`}
          className='bg-purple-600 text-white w-full p-6 inline-flex gap-3 items-center rounded-md'
        >
          <ArrowLeftOutlined className='text-[22px]' />
          <h4 className='font-semibold text-lg'>Account Details</h4>
        </Link>

        {/* avatar */}
        <div className='flex items-center h-52 justify-center my-10 flex-col'>
          <Image
            src={AVATAR}
            width={150}
            height={150}
            loading='lazy'
            className='object-cover rounded'
            alt='account name here'
          ></Image>
          <h1 className='font-bold text-lg mt-3'>Surfiya Zakir</h1>
          <p className='text-[#adb5bd] mt-1 text-sm font-medium'>
            New York City
          </p>
        </div>

        {/* form */}
        <form autoComplete='off' className='px-6 pb-6 flex flex-col gap-4'>
          <div className='grid lg:grid-cols-2 grid-cols-1 gap-x-5 gap-y-4'>
            {fakeFormData.map((item) => (
              <div key={item.id} className='form-group'>
                <Label
                  classnames='!text-[#515184] font-semibold text-sm'
                  htmlFor={item.inputName}
                >
                  {item.title}
                </Label>
                <Input
                  id={item.inputName}
                  name={item.inputName}
                  placeholder={`Add your ${item.inputName}...`}
                />
              </div>
            ))}
          </div>

          <div className='grid grid-cols-1'>
            <div className='form-group'>
              <Label
                classnames='!text-[#515184] font-semibold text-sm'
                htmlFor='desc'
              >
                Description
              </Label>
              <Textarea
                id='desc'
                rows={5}
                placeholder='Write something about you...'
              />
            </div>
          </div>

          <Button type='submit' color='secondary' classnames='w-[175px]'>
            Save
          </Button>
        </form>
      </article>
    </section>
  )
}
