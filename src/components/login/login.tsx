'use client'
import Image from 'next/image'
import Link from 'next/link'
import { Button, Checkbox, Input, Label } from '../common'
import { BANNER, messageValidate } from '@/constants'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import { LoginValidate } from '@/types'
const { ISEMAIL, MIN, REQUIRE } = messageValidate

const schema = yup.object({
  email: yup.string().required(REQUIRE).email(ISEMAIL),
  password: yup.string().required(REQUIRE).min(5, MIN),
})

export default function LoginComponent() {
  const {
    register,
    handleSubmit,
    formState: { errors, isSubmitting },
  } = useForm<LoginValidate>({
    resolver: yupResolver(schema),
  })

  const onLogin = (value: LoginValidate) => {
    console.log(value)
  }
  return (
    <section className='min-h-screen'>
      <div className='flex'>
        <div className='lg:w-[40%] w-0 relative min-h-screen'>
          <Image
            src={BANNER}
            fill
            alt='auth banner'
            sizes='(max-width: 1024px) 0, 40vw'
            className='object-cover'
          ></Image>
        </div>

        <div className='grid w-full place-items-center lg:w-[60%] min-h-screen'>
          <div className='hero'>
            <h1 className='text-4xl font-bold text-black text-start'>
              Login into <br />
              your accounts
            </h1>

            <form
              onSubmit={handleSubmit(onLogin)}
              className='flex flex-col mt-5 gap-y-4'
              autoComplete='off'
            >
              <Input
                classnames='md:w-96'
                type='email'
                placeholder='Your email'
                {...register('email')}
              />
              {errors && errors?.email && (
                <p className='text-red-500 text-sm'>{errors?.email.message}</p>
              )}

              <Input
                classnames='md:w-96'
                type='password'
                placeholder='Your password'
                {...register('password')}
              />

              {errors && errors?.password && (
                <p className='text-red-500 text-sm'>
                  {errors?.password.message}
                </p>
              )}

              <div className='flex items-center justify-between'>
                <div className='flex items-center gap-3 form-group'>
                  <Checkbox name='remember' id='remember' />
                  <Label htmlFor='remember'>Remember me</Label>
                </div>

                <span className='font-semibold text-sm text-[#495057] hover:text-blue-400'>
                  <Link href='/forgot'>Forgot your password?</Link>
                </span>
              </div>

              <Button disabled={isSubmitting}>Login</Button>
            </form>
            <p className='text-[#adb5bd] font-light mt-3 text-sm'>
              Dont have an accounts?{' '}
              <Link className='font-bold text-purple-500' href='/register'>
                Register an accounts
              </Link>
            </p>

            <div className='flex flex-col gap-3 mt-3'>
              <p className='font-light text-center text-[#adb5bd] text-sm'>
                Or login with your social accounts
              </p>

              <Button color='secondary'>Login with Goggle</Button>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}
