import { Author, Message, ChatInput } from '@/components/common'
import { fakeChats } from '@/constants/static'
import { WidgetHeader } from '..'

export default function ChatComponent() {
  return (
    <article className='max-w-full bg-[#c9c9d6] mt-24 min-h-screen flex flex-col items-center justify-center'>
      {/* header */}
      <WidgetHeader />

      <section className='w-[800px] mt-2 overflow-y-auto md:mx-0 mx-4 max-h-auto bg-white rounded-md py-10 px-8'>
        {/* message zone */}
        <div className='flex flex-col message-info gap-y-1 last:mb-11 first:mt-12'>
          {fakeChats.map((chat) => (
            <div
              key={chat.id}
              className={`flex flex-col gap-y-2 ${chat.isSender && 'ml-auto'}`}
            >
              <Author
                isSender={chat.isSender}
                size='chat'
                name={chat.name}
                time={chat.createdAt}
              />
              <Message isSender={chat.isSender}>{chat.content}</Message>
            </div>
          ))}
        </div>
      </section>

      <div className='fixed bottom-0 shadow-lg bg-[#fff] w-full py-4 max-w-[800px]'>
        <ChatInput></ChatInput>
      </div>
    </article>
  )
}
