import { AVATAR } from '@/constants'
import { BellOutlined, HeartOutlined, SearchOutlined } from '@ant-design/icons'

/* eslint-disable @next/next/no-img-element */
export default function WidgetHeader() {
  return (
    <div className='flex fixed z-10 p-2.5 w-full backdrop-blur-xl max-w-[800px] backdrop:filter-none bg-transparent shadow-md top-[100px] sm:items-center justify-between border-b-2 rounded-lg border-gray-200'>
      <div className='relative flex items-center space-x-4'>
        <div className='relative'>
          <span className='absolute text-green-500 right-0 bottom-0'>
            <svg width='20' height='20'>
              <circle cx='8' cy='8' r='8' fill='currentColor'></circle>
            </svg>
          </span>
          <img
            src={AVATAR}
            alt='avatar'
            className='w-10 sm:w-16 h-10 sm:h-16 rounded-full'
          />
        </div>
        <div className='flex flex-col leading-tight'>
          <div className='text-2xl mt-1 flex items-center'>
            <span className='text-gray-700 mr-3'>Bob Smith</span>
          </div>
          <span className='text-sm text-gray-300'>Active</span>
        </div>
      </div>
      <div className='flex items-center space-x-2'>
        <button
          type='button'
          className='inline-flex items-center text-lg justify-center rounded-lg border h-10 w-10 transition duration-500 ease-in-out text-gray-500 hover:bg-gray-300 focus:outline-none'
        >
          <SearchOutlined />
        </button>
        <button
          type='button'
          className='inline-flex items-center text-lg justify-center rounded-lg border h-10 w-10 transition duration-500 ease-in-out text-gray-500 hover:bg-gray-300 focus:outline-none'
        >
          <HeartOutlined />
        </button>
        <button
          type='button'
          className='inline-flex items-center text-lg justify-center rounded-lg border h-10 w-10 transition duration-500 ease-in-out text-gray-500 hover:bg-gray-300 focus:outline-none'
        >
          <BellOutlined />
        </button>
      </div>
    </div>
  )
}
