import { CheckOutlined, DeleteOutlined } from '@ant-design/icons'
import { Author } from '../common'
import Link from 'next/link'

export default function MessageComponent() {
  return (
    <article className='max-w-full bg-[#fbfcfe] mt-24 min-h-screen flex flex-col items-center justify-center'>
      <section className='w-[800px] mt-2 overflow-y-auto md:mx-0 mx-4 max-h-auto bg-white rounded-md py-10 px-8'>
        {/* heading */}
        <div className='flex font-bold items-center gap-1 mb-4'>
          <h1 className='text-xl text-gray-900'>My message</h1>
          <span className='p-2 bg-orange-400 text-white rounded-md'>25</span>
        </div>

        {/* message hero */}
        <div className='flex flex-col gap-y-3'>
          {Array(10)
            .fill(null)
            .map((_, index) => (
              <div
                key={index}
                className={`${
                  index % 2 >= 1 && 'bg-blue-100'
                } flex hover:bg-red-100 cursor-pointer items-center justify-between gap-2 bg-white rounded-md p-2`}
              >
                <Link
                  href={`message/${index + 1}`}
                  className='flex items-start gap-2'
                >
                  <Author name='Athur Smith' time='3 minutes ago' />
                  <p className='text-sm font-semibold mt-1'>
                    messaged you. Check it now
                  </p>
                </Link>

                <div className='flex items-center gap-2'>
                  <span className='text-lg rounded-md flex items-center justify-center text-white p-3 cursor-pointer bg-gray-300 hover:bg-gray-500'>
                    <CheckOutlined />
                  </span>

                  <span className='text-lg rounded-md flex items-center justify-center text-white p-3 cursor-pointer bg-gray-300 hover:bg-gray-500'>
                    <DeleteOutlined />
                  </span>
                </div>
              </div>
            ))}
        </div>
      </section>
    </article>
  )
}
