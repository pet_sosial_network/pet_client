'use client'
import Image from 'next/image'
import Link from 'next/link'
import { Input, Label, Checkbox, Button } from '../common'
import { BANNER, regex, messageValidate } from '@/constants'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import { RegisterValidate } from '@/types'

const { ISEMAIL, MIN, REQUIRE, REGEX, CHECK, MATCH } = messageValidate

const schema = yup.object({
  fullname: yup.string().required(REQUIRE).matches(regex, REGEX),
  email: yup.string().required(REQUIRE).email(ISEMAIL),
  password: yup.string().required(REQUIRE).min(5, MIN),
  confirm: yup
    .string()
    .required(REQUIRE)
    .oneOf([yup.ref('password')], MATCH),
  terms: yup.boolean().required(REQUIRE).oneOf([true], CHECK),
})

export default function RegisterComponents() {
  const {
    handleSubmit,
    register,
    formState: { errors, isSubmitting },
  } = useForm<RegisterValidate>({
    resolver: yupResolver(schema),
  })

  const onRegister = (value: RegisterValidate) => {
    console.log(value)
  }

  return (
    <section className='min-h-screen'>
      <div className='flex'>
        <div className='lg:w-[40%] w-0 relative min-h-screen'>
          <Image
            src={BANNER}
            fill
            alt='auth banner'
            sizes='(max-width: 1024px) 0, 40vw'
            className='object-cover'
          ></Image>
        </div>
        <div className='grid place-items-center lg:w-[60%] w-full min-h-screen'>
          <div className='hero'>
            <h1 className='text-4xl font-bold text-black text-start'>
              Create <br />
              your accounts
            </h1>
            <form
              onSubmit={handleSubmit(onRegister)}
              className='flex flex-col mt-5 gap-y-4'
              autoComplete='off'
            >
              <Input
                classnames='md:w-96'
                {...register('fullname')}
                placeholder='Your name'
              />
              {errors && errors?.fullname && (
                <p className='text-red-500 text-sm'>
                  {errors?.fullname.message}
                </p>
              )}

              <Input
                classnames='md:w-96'
                {...register('email')}
                type='email'
                placeholder='Your email'
              />
              {errors && errors?.email && (
                <p className='text-red-500 text-sm'>{errors?.email.message}</p>
              )}

              <Input
                classnames='md:w-96'
                type='password'
                placeholder='Your password'
                {...register('password')}
              />
              {errors && errors?.password && (
                <p className='text-red-500 text-sm'>
                  {errors?.password.message}
                </p>
              )}

              <Input
                classnames='md:w-96'
                type='password'
                placeholder='Comfirm password'
                {...register('confirm')}
              />

              {errors && errors?.confirm && (
                <p className='text-red-500 text-sm'>
                  {errors?.confirm.message}
                </p>
              )}

              <div className='flex items-center gap-3 form-group'>
                <Checkbox id='terms' {...register('terms')} />
                <Label
                  htmlFor='terms'
                  classnames={errors.terms ? 'text-red-500' : ''}
                >
                  Accept Term and Conditions
                </Label>
              </div>

              <Button disabled={isSubmitting} type='submit'>
                Register
              </Button>
            </form>
            <p className='text-[#adb5bd] font-light mt-3 text-sm'>
              Already have an accounts?{' '}
              <Link className='font-bold text-purple-600' href='/login'>
                Login now
              </Link>
            </p>
          </div>
        </div>
      </div>
    </section>
  )
}
