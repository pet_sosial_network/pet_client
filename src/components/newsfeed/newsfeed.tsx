'use client'
import { COVER_IMG } from "@/constants"
import Image from 'next/image'
import { CreatedPost, PostedComponent, Friend, UserLive, UserFollow} from "../common"
import { LoadingOutlined, LikeOutlined } from '@ant-design/icons'
import Link from "next/link"

export default function NewsfeedComponent() {
    const rightBlockCss = 'w-full md:w-[300px] bg-white shadow-md mt-10 rounded-xl text-sm'
    const leftBlockCss = 'w-[640px] mb-4'
    const seeAllCss = 'text-sky-600 md:ml-[30%] font-bold py-3 ml-[60%]'
    const titleBlockCss = 'font-bold ml-6 mt-3'
    return (     
        <section className='bg-gray-100 flex md:flex-row flex-col min-h-screen justify-center max-w-full md:pl-[25%] sm:pl-[5%]'>
            <div>
                {/* create post and posted component*/}
                <div className ={`${leftBlockCss} flex flex-col gap-4 mt-28`}>
                    <CreatedPost/>
                    <PostedComponent/>
                </div>

                {/* user live */}
                <div className={leftBlockCss}>
                    <UserLive/>
                </div>
                {/* postedComponent */}
                <div className ={leftBlockCss}>
                    <PostedComponent/>
                </div>

                {/* user follow */}
                <div className={leftBlockCss}>
                    <UserFollow/>
                </div>
                {/* postedComponent */}
                <div className ={leftBlockCss}>
                    <PostedComponent/>
                </div>

                {/* loading */}
                <div className ='w-[640px] md:w-full text-center bg-white shadow-md rounded-xl p-2'>
                    <LoadingOutlined />    
                </div>
            </div>
           
            <div className="md:ml-4 w-[640px]">
                {/* friend request */}
                <div className={`${rightBlockCss} md:mt-28`}>
                    <div className="flex p-2">
                        <h4 className={titleBlockCss}>Friend Request</h4>
                        <span className={seeAllCss}><Link href="">See all</Link></span>
                    </div>
                    <hr/>     
                    <Friend/>
                </div>
                     
                {/* suggest pages */}
                <div className={rightBlockCss}>
                    <div className="flex p-2">
                        <h4 className={titleBlockCss}>Suggest Pages</h4>
                        <span className={seeAllCss}><Link href="">See all</Link></span>
                    </div>
                    <hr/>
                    <div className="ml-4 mt-5">
                        <Image 
                            src={COVER_IMG} 
                            alt="suggest-page"
                            width={270}
                            height={260}
                            className="rounded-xl w-[60%] md:w-[90%]"
                        >
                        </Image>
                        <button className="p-4 bg-gray-300 rounded-full text-xs w-[95%] mt-3 mb-5">
                            <LikeOutlined className="text-base"/>
                            <span className="font-bold ml-2">Like Page</span>
                        </button>
                    </div>
                </div>

                {/* suggest group */}
                <div className={rightBlockCss}>
                    <div className="flex p-2">
                        <h4 className={titleBlockCss}>Suggest Group</h4>
                        <span className={seeAllCss}><Link href="">See all</Link></span>
                    </div>
                    <hr/>
                    <div className="ml-4 mt-5">
                        <Image 
                            src='/images/group.jpg' 
                            alt="suggest-group"
                            width={270}
                            height={260}
                            className="rounded-xl object-cover w-[60%] md:w-[90%]"
                        >
                        </Image>
                        <div className="flex -space-x-4 pb-10 mt-3">
                            <Image className= 'z-0'src='/images/user/user-2.png' alt="" width={40} height={40}></Image>
                            <Image className= 'z-20'src='/images/user/user-3.png' alt="" width={40} height={40}></Image>
                            <Image className= 'z-30'src='/images/user/user-4.png' alt="" width={40} height={40}></Image>
                            <div className="z-50 rounded-full bg-slate-200 p-3 text-gray-400 pb-2 font-bold">+2</div>
                            <span className="z-50 text-gray-300 font-bold pl-5 mt-3 text-[13px]">Member apply</span>
                        </div>
                    </div>
                </div>

                {/* event */}
                <div className={`${rightBlockCss} pb-4`}>
                    <div className="flex p-2">
                        <h4 className={titleBlockCss}>Event</h4>
                        <span className={`${seeAllCss} md:ml-[53%] ml-[70%]`}><Link href="">See all</Link></span>
                    </div>
                   {Array(3)
                    .fill(0)
                    .map((_, index)=> (
                        <div key={index} className="flex p-2 ml-4">
                            <div className="text-xl mr-2 p-2 bg-orange-400 rounded-xl text-white text-center font-bold">
                                <span className="uppercase text-xs -mb-2 p-2">feb</span>
                                <p>22</p>
                            </div>
                            <div className="mt-2">
                                <h4 className="font-bold text-sm">Meeting with clients</h4>
                                <p className="text-gray-400 text-xs">Lorem ipsum dolor sit amet</p>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </section>
    )
}