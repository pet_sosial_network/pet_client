import { ChatComponent } from '@/components/messages'
import { Metadata } from 'next'

export const metadata: Metadata = {
  title: 'Private message',
  description: 'Private chat message',
}

export default function PrivateChat() {
  return <ChatComponent />
}
