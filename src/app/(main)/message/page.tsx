import { MessageComponent } from '@/components/messages'
import { Metadata } from 'next'

export const metadata: Metadata = {
  title: 'My message',
  description: 'Message page',
}
export default function Message() {
  return <MessageComponent />
}
