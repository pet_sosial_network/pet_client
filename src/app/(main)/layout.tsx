'use client'
import { Header } from '@/components/common'
import { AppProgressBar as ProgressBar } from 'next-nprogress-bar'

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <>
      <Header />
      <main>
        <ProgressBar
          height='4px'
          color='#009dff'
          options={{ showSpinner: false }}
          shallowRouting
        />
        {children}
      </main>
    </>
  )
}
