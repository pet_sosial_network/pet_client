import SettingsComponents from '@/components/settings'
import { Metadata } from 'next'

export const metadata: Metadata = {
  title: 'Settings',
  description: 'Settings with your accouts',
}

export default function Settings() {
  return <SettingsComponents />
}
