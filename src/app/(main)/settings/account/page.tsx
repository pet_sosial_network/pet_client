import AccountComponent from '@/components/account'
import { Metadata } from 'next'

export const metadata: Metadata = {
  title: 'Update your accounts',
  description: 'Update accounts infomation',
}

export default function Account() {
  return <AccountComponent />
}
