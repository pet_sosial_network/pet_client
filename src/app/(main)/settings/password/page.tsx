import PasswordComponent from '@/components/password'
import { Metadata } from 'next'

export const metadata: Metadata = {
  title: 'Change your passwsord accounts',
  description: 'Change password pages',
}

export default function ChangePassword() {
  return <PasswordComponent />
}
