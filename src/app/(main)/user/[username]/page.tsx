export default function User({ params }: { params: { username: string } }) {
  return (
    <main className='flex min-h-screen items-center justify-center'>
      <h2 className='text-3xl font-bold text-blue-300'>
        User {params.username}
      </h2>
    </main>
  )
}
