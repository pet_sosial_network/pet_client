import ProfileComponent from '@/components/profile'
import { Metadata } from 'next'

export const metadata: Metadata = {
  title: 'My profile',
  description: 'Page show infomation of user',
}

export default function Profile() {
  return <ProfileComponent />
}
