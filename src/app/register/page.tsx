import RegisterComponents from '@/components/register'
import { Metadata } from 'next'

export const metadata: Metadata = {
  title: 'Register an accounts',
  description: 'Register page pet project',
}

export default function Register() {
  return <RegisterComponents />
}
