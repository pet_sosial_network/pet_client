import { NOTFOUND } from '@/constants'
import { Metadata } from 'next'
import Image from 'next/image'
import Link from 'next/link'

export const metadata: Metadata = {
  title: 'Not found this page',
}

export default function NotFound() {
  return (
    <section className='min-h-screen flex items-center justify-center'>
      <div className='text-center px-4'>
        <Image
          src={NOTFOUND}
          alt='not found page'
          width={200}
          height={100}
          className='w-52 h-full object-cover mx-auto mb-4'
        ></Image>
        <h1 className='font-bold lg:text-5xl text-[40px] leading-[1.2] mb-3'>
          Opps,it look like <br /> you are lost
        </h1>
        <p className='text-[#adb5bd] text-sm mb-4'>
          The page you are looking for is not available. Try to search again or{' '}
          <br />
          use the go to.
        </p>

        <Link
          href='/'
          className='font-semibold text-white p-4 w-44 rounded inline-block bg-purple-600'
        >
          Home Page
        </Link>
      </div>
    </section>
  )
}
