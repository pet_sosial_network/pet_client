import LoginComponent from '@/components/login'
import { Metadata } from 'next'
import React from 'react'

export const metadata: Metadata = {
  title: 'Login with your account',
  description: 'Login pages pet project',
}

export default function Login() {
  return <LoginComponent />
}
