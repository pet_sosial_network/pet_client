import { MenuStatics } from '@/types'

export const BANNER = '/images/banner/login-bg.jpg'
export const NOTFOUND = '/images/banner/bg-notfound.png'
export const AVATAR = '/images/avatar.jpg'
export const COVER_IMG = '/images/banner/bb.jpg'

export const messageValidate = {
  REQUIRE: 'This field is required',
  ISEMAIL: 'This field must be an email',
  REGEX: 'Does not contain special characters',
  MIN: 'Must be more than 5 characters',
  CHECK: 'Please check the box',
  MATCH: 'Password does not match',
}

export const fakeFormData = [
  { id: 1, title: 'First Name', inputName: 'firstname' },
  { id: 2, title: 'Last Name', inputName: 'lastname' },
  { id: 3, title: 'Email', inputName: 'email' },
  { id: 4, title: 'Phone', inputName: 'phone' },
  { id: 5, title: 'Town/City', inputName: 'city' },
  { id: 6, title: 'Addr', inputName: 'address' },
]

export const menuProfileList: Omit<MenuStatics, 'icon'>[] = [
  { id: 1, title: 'About', href: 'about' },
  { id: 2, title: 'Membership', href: 'membership' },
  { id: 3, title: 'Discusion', href: 'discusion' },
  { id: 4, title: 'Group', href: 'group' },
  { id: 5, title: 'Video', href: 'video' },
  { id: 6, title: 'Media', href: 'media' },
]

export const regex = /^[a-zA-Z0-9]{0,10}$/
