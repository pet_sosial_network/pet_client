import { MenuStatics } from '@/types'
import { AVATAR, COVER_IMG } from '@/constants'
import {
  BellOutlined,
  CreditCardOutlined,
  EnvironmentOutlined,
  HomeOutlined,
  KeyOutlined,
  MessageOutlined,
  SettingOutlined,
  TwitterOutlined,
  WarningOutlined,
} from '@ant-design/icons'


export const settingsMenulist = {
  General: [
    {
      id: 1,
      title: 'Account Infomation',
      href: 'account',
      icon: <HomeOutlined />,
    },
    {
      id: 2,
      title: 'Saved Address',
      href: 'addr',
      icon: <EnvironmentOutlined />,
    },
    {
      id: 3,
      title: 'Account Social',
      href: 'social',
      icon: <TwitterOutlined />,
    },
  ],
  Account: [
    { id: 1, title: 'My Cards', href: 'my-card', icon: <CreditCardOutlined /> },
    { id: 2, title: 'Passwsord', href: 'password', icon: <KeyOutlined /> },
  ],
  Other: [
    { id: 1, title: 'Notifications', href: 'noti', icon: <BellOutlined /> },
    { id: 2, title: 'Logout', href: 'logout', icon: <WarningOutlined /> },
  ],
}

export const navBarActions: MenuStatics[] = [
  { id: 1, title: 'Noti', icon: <BellOutlined />, href: '/noti' },
  { id: 2, title: 'Message', icon: <MessageOutlined />, href: '/message' },
  { id: 3, title: 'Settings', icon: <SettingOutlined />, href: '/settings' },
]

export const profileSubMenu: MenuStatics[] = [
  { id: 1, title: 'My Profile', icon: <BellOutlined />, href: '/profile' },
  { id: 2, title: 'Message', icon: <MessageOutlined />, href: '/message' },
  { id: 3, title: 'Settings', icon: <SettingOutlined />, href: '/settings' },
  { id: 4, title: 'Logout', href: '/logout', icon: <WarningOutlined /> },
]

export const fakeChats = [
  {
    id: 'ABCDE',
    content: 'Lorem ipsum dolor sit amet',
    isSender: true,
    name: 'Alice Johnson',
    createdAt: '21:25:45',
  },
  {
    id: 'FGHIJ',
    content: 'Consectetur adipiscing elit',
    isSender: false,
    name: 'Bob Smith',
    createdAt: '04:50:12',
  },
  {
    id: 'KLMNO',
    content: 'Sed do eiusmod tempor incididunt',
    isSender: true,
    name: 'Alice Johnson',
    createdAt: '22:17:36',
  },
  {
    id: 'PQRST',
    content: 'Ut labore et dolore magna aliqua',
    isSender: false,
    name: 'Bob Smith',
    createdAt: '07:30:59',
  },
  {
    id: 'UVWXY',
    content: 'Ut enim ad minim veniam',
    isSender: true,
    name: 'Alice Johnson',
    createdAt: '23:48:03',
  },
  {
    id: 'Z1234',
    content: 'Quis nostrud exercitation ullamco',
    isSender: false,
    name: 'Bob Smith',
    createdAt: '02:15:27',
  },
  {
    id: '56789',
    content: 'Duis aute irure dolor in reprehenderit',
    isSender: true,
    name: 'Alice Johnson',
    createdAt: '18:02:11',
  },
]

export const fakeUserLiveInfo = [
  {
    avatar: '/images/user/user-2.png',
    background: '/images/bgUser/bg1.jpg',
    name: 'Aliqa Macale' ,
    gmail: 'aliqa@gmail.com'
  },
  {
    avatar: '/images/user/user-3.png',
    background: '/images/bgUser/bg2.png',
    name: 'Seary Victor',
    gmail: 'seary@gmail.com'
  },
  {
    avatar: '/images/user/user-4.png',
    background: '/images/bgUser/bg3.jpg',
    name: 'John Smith',
    gmail: 'john@gmail.com'
  },
  { 
    avatar: AVATAR,
    background: COVER_IMG,
    name: 'Linda Enterprise',
    gmail: 'linda@gmail.com'
  },
  {
    avatar: '/images/user/user-2.png',
    background: '/images/bgUser/bg4.png',
    name: 'Studio Express',
    gmail: 'studios@gmail.com'
  },
  {
    avatar: '/images/user/user-3.png',
    background: '/images/bgUser/bg1.jpg',
    name: 'Vitory Lite',
    gmail: 'lisa@gmail.com'
  }
]


export const fakeUserFollowInfo = [
  {
    avatar: AVATAR,
    name: 'Aliqa Macale' ,
    id: '@macale341'
  },
  {
    avatar: '/images/user/user-4.png',
    name: 'Seary Victor',
    id: '@macale342'
  },
  {
    avatar: '/images/user/user-2.png',
    name: 'John Smith',
    id: '@macale343'
  },
  {
    avatar: '/images/user/user-3.png',
    name: 'Studio Express',
    id: '@macale344'
  },
  {
    avatar: AVATAR,
    name: 'Lisa Lite',
    id: '@macale345'
  },
  {
    avatar: '/images/user/user-2.png',
    name: 'Linda Enterprise',
    id: '@macale346'
  }
]

export const fakeListFriends = [
  {
    avatar: '/images/user/user-4.png',
    name: 'Anthony Daugloi',
    numMutualFriends: 12
  },
  {
    avatar: '/images/user/user-2.png',
    name: 'Mohannad Zitoun',
    numMutualFriends: 52
  },
  {
    avatar: '/images/user/user-3.png',
    name: 'Mohannad Zitoun',
    numMutualFriends: 6
  },
]