import { ReactNode } from 'react'

export type InputProps = {
  classnames?: string
  children?: React.ReactNode
} & JSX.IntrinsicElements['input']

export type RegisterValidate = {
  fullname: string
  email: string
  password: string
  confirm: string
  terms: boolean
}

export type LoginValidate = {
  email: string
  password: string
}
export type MenuStatics = {
  id: number
  title: string
  icon: ReactNode
  href: string
}
