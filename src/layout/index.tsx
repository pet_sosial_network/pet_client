'use client'
import { Header } from '@/components/common'
import { usePathname } from 'next/navigation'
import { Fragment } from 'react'

type LayoutProps = {
  children: React.ReactNode
}

const byPassURL = ['/login', '/register']

const MainLayout = ({ children }: LayoutProps) => {
  const pathname = usePathname()
  const noHeader = byPassURL.some((url) => url === pathname)
  return (
    <main>
      {noHeader ? <Fragment /> : <Header />}
      {children}
    </main>
  )
}

export default MainLayout
